import request from '~/service';

export function getIas () {
    const data = {};
    const user = sessionStorage.getItem('loginUser');
    return request.post(`/AERC/rest/Ia/${user}`, data);
}
    
export function getMngs (Ia) {
    const data = Ia ? { Ia: Ia } : {};
    return request.post('/AERC/rest/mng', data);
}
    
export function getStns (Ia, Mng) {
    const data = Mng !== '' ? { Ia: Ia, Mng: Mng } : { Ia: Ia };
    return request.post('/AERC/rest/Stn', data);
}
    
export function getGrps (Ia, Mng, Stn) {
    const data = Mng !== '' ? { Ia: Ia, Mng: Mng, Stn: Stn } : { Ia: Ia, Stn: Stn };
    return request.post('/AERC/rest/Grp', data);
}

export function getCanal (data) {
    return request.post('/AERC/rest/Canal', data);
}

export function getCounties (data) {
    return request.post('/AERC/rest/County', data);
}

export function getTowns (county,fid) {
    const data = county ? { CountyID: county } : {};
    if(fid){
        data.fid = fid;
    }
    return request.post('/AERC/rest/Town', data);
}

export function getSections (county, town) {
    const data = {
        CountyID: county,
        TownID: town || ''
    };
    return request.post('/AERC/rest/Section', data);
}

export function getLands (county, town, section) {
    const q = [];
    if (county) q.push(`CountyID=${county}`);
    if (town) q.push(`TownID=${town}`);
    if (section) q.push(`Section=${section}`);
    return request.post('/AERC/rest/Sec5cov', (q ? q.join('&') : ''));
}

export function getSec5cov (data) {
    const secNoRequest = `/AERC/rest/Sec5cov?pageCnt=1&pageRows=1`;
    return request.post(secNoRequest,data);
}

export function getSecNo (county, section) {
    const secNoRequest = `/AERC/rest/Sec5no?CountyID=${county}&Section=${section}`;
    return request.get(secNoRequest);
}

export function getSecNoList (county, section) {
    const secNoListRequest = `/AERC/rest/Sec5nos?CountyID=${county}&Section=${section}`;
    return request.get(secNoListRequest);
}

export function getSecFid (county, fid) {
    const secFidRequest = `/AERC/rest/Sec5ByFID?CountyID=${county}&FID=${fid}`;
    return request.get(secFidRequest);
}

export function searchIrrigationLandArea (data) {
    return request.post('/AERC/rest/IrrigationLand', data);
}

export function getLayer () {
    return request.get('/AERC/rest/Layer');
}

export function getIRLayer () {
    return request.get('/AERC/rest/IRLayer');
}

export function getPWALayer () {
    return request.get('/AERC/rest/PWALayer');
}

export function getRalidSection (data) {
    return request.get(`/AERC/rest/RalidSection?CountyID=${data.county}&Office=${data.office}`);
}

export function getRalidLandno (data) {
    return request.get(`/AERC/rest/RalidLandno?CountyID=${data.county}&Section=${data.section}`);
}

