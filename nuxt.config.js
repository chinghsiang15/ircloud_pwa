import path from 'path'
import fs from 'fs'
export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  mode: 'spa',
  vue: {
    config: {
      devtools: true,
      productionTip: false
    }
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  router: {
    base: '/IRCloud_PWA/'
  },

  head: {
    title: 'IRCloud_PWA',
    htmlAttrs: {
      lang: 'zh-TW'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    script: [
      {
        src: '/IRCloud_PWA/scripts/Map.js',
        async: true
      },
      {
        src: '/IRCloud_PWA/scripts/ToolControls.js',
        async: true
      },
      {
        src: '/IRCloud_PWA/scripts/setMap.js',
        async: true
      },
      // 加密套件 //
      {
        src: '/AERC/Scripts/components/core-min.js',
        async: true
      },
      {
        src: '/AERC/Scripts/rollups/aes.js',
        async: true
      },
      {
        src: '/AERC/Scripts/rollups/sha256.js',
        async: true
      },
      {
        src: 'https://cdnjs.cloudflare.com/ajax/libs/x2js/1.2.0/xml2json.min.js',
        async: true
      }
      // 加密套件 //
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/IRCloud_PWA/static/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'vant/lib/index.css',
    'vant/lib/index.less',
    { src: '~assets/scss/main.scss', lang: 'scss' }
  ],

  styleResources: {
    scss: ['./assets/scss/*.scss']
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/vant',
    '@/plugins/vuescroll',
    '@/plugins/select',
    '@/plugins/font',
    '@/plugins/touch',
    '@/plugins/mixin'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/pwa",
    "@nuxtjs/axios",
    "nuxt-webfontloader",
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
		loaders: {
      less: {
        javascriptEnabled: true,
        modifyVars: {
          '@tabbar-background-color': '#3E889F',
          '@tabbar-item-active-background-color': '#1F6473',
          '@tabbar-height':'65px',
          '@dropdown-menu-content-max-height' : '50%',
          '@dropdown-menu-height' : '36px',
          '@toast-text-color' : '#D22626',
          '@toast-background-color' : 'fade(#FFE5E5, 100%)',
          '@toast-border-radius' : '0px',
          '@field-placeholder-text-color' : '#959595',
        }
      },
      extend (config, { isDev, isClient }) {
        if (!isDev) {
          config.output.publicPath = './_nuxt/';
        }
        return config;
      },
      filenames: {
        app: ({ isDev }) => isDev ? '[name].js' : '[chunkhash].js',
        chunk: ({ isDev }) => isDev ? '[name].js' : '[chunkhash].js',
        img: ({ isDev }) => isDev ? '[path][name].[ext]' : 'img/[hash:7].[ext]',
      }
    }
  },

  server: {
    host: 'localhost'
    //port: 8000, // default: 3000
    //host: '0.0.0.0', // default: localhost
    // https: {
    //   key: fs.readFileSync(path.resolve(__dirname, 'localhost-key.pem')),
    //   cert: fs.readFileSync(path.resolve(__dirname, 'localhost.pem'))
    // }
  },

  pwa: {
    meta: {
      title: 'My PWA',
      author: 'Me',
    },
    manifest: {
      name: '農田水利現勘應用工具(PWA)',
      short_name: '農田水利現勘應用工具(PWA)',
      lang: 'zh-TW',
      icons:[
        {
          "src": "/IRCloud_PWA/icon_144x144.png",
          "sizes": "144x144",
          "type": "image/png"
        },
        {
          "src": "/IRCloud_PWA/icon_152x152.png",
          "sizes": "152x152",
          "type": "image/png"
        },
        {
          "src": "/IRCloud_PWA/icon_192x192.png",
          "sizes": "192x192",
          "type": "image/png"
        },
        {
          "src": "/IRCloud_PWA/icon_512x512.png",
          "sizes": "512x512",
          "type": "image/png"
        }
      ],
      start_url: "https://www.iacloud.ia.gov.tw/IRCloud_PWA_test/login",
      display: "standalone",
      background_color: "#fff",
      theme_color: "#1976d2",
      scope:'https://www.iacloud.ia.gov.tw/IRCloud_PWA/'
    }
  },

  webfontloader: {
    google: {
      families: ['Noto Sans TC']
    }
  }
}
