export const state = () => ({
    userInfo: {},
    authenticated: false,
    //預設裝置觸發事件
    deviceEvent:'touchstart',
    clearMapDraw:false,
    mapObj:null,
    dmapObj:null,
    picData: {},
    selectLayerOption:{
        surfaceLayer:[],
        imageLayer:[],
        areaLayer:[],
    },
    selectSearchLayer:['管理處'],
    surfaceList:[],
    pointList:[],
    lineList:[],
    //sec5CovList:[],
    count: {
        Cons: 0,
        Canal: 0,
        Ia: 1,
        Stn: 0,
        Grp: 0,
        Sec5cov: 0
    },
    pwaLayerList:[],
    mapOpacity:100,
    mapAction:'',
    maskStatus: false
})

export const mutations = {
    SET_USER_INFO (state, payload) {
        state.userInfo = payload.userInfo;
    },
    SET_PIC_DATA (state, payload) {
        state.picData = payload;
    },
    CLEAR_SELECT_LAYER (state) {
        state.selectSearchLayer = ['管理處'];
    },
    SET_SELECT_LAYER (state, payload) {
        state.selectLayerOption[payload.obj] = payload.arr;
    },
    SET_SEARCH_LAYER (state, payload) {
        state.selectSearchLayer.push(payload);
        state.selectSearchLayer = [...(new Set(state.selectSearchLayer))];
    },
    DEL_SEARCH_LAYER (state, payload) {
        state.selectSearchLayer = [...(new Set(state.selectSearchLayer))];
        const _id = state.selectSearchLayer.indexOf(payload);
        state.selectSearchLayer.splice(_id,1);
    },
    SET_LAYER_SURFACE_LIST(state, payload) {
        state.surfaceList = payload;
    },
    SET_LAYER_POINT_LIST(state, payload) {
        state.pointList = payload;
    },
    SET_LAYER_LINE_LIST(state, payload) {
        state.lineList = payload;
    },
    // SET_LAYER_SEC5COV_LIST(state, payload) {
    //     state.sec5CovList = payload;
    // },
    SET_COUNT_IA (state) {
        state.count.Ia++;
    },
    SET_COUNT_STN (state) {
        state.count.Stn++;
    },
    SET_COUNT_GRP (state) {
        state.count.Grp++;
    },
    SET_COUNT_CONS (state) {
        state.count.Cons++;
    },
    SET_COUNT_CANAL (state) {
        state.count.Canal++;
    },
    // SET_COUNT_SEC5COV (state) {
    //     state.count.Sec5cov++;
    // },
    SET_PWALAYER_LIST (state,payload) {
        state.pwaLayerList = payload;
    },
    SET_MAP_OPACITY(state,payload){
        state.mapOpacity = payload
    },
    SET_MAP_ACTION(state,payload){
        state.mapAction = payload
    },
    SET_MASK_STATUS(state,payload){
        state.maskStatus = payload
    },
    CLEAR_MAP_DRAW(state,payload){
        state.clearMapDraw = payload
    },
    SET_MAP_OBJ(state,payload){
        state.mapObj = payload
    },
    SET_DMAP_OBJ(state,payload){
        state.dmapObj = payload
    },
}