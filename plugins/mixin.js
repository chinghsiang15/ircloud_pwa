import Vue from 'vue';

Vue.mixin({
    methods: {
        // * 千分位符號
        GET_COMMA (payload) {
        if (payload !== undefined && payload !== null && payload !== '') {
            const re = new RegExp("(\\d{1,3})(?=(\\d{3})+(?:$|\\D))", "g");
            return payload?.toString().replace(re, "$1,");
        } else {
            return payload;
        }
        }
    }
});