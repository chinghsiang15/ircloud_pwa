import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFolder,faAngleLeft,faAngleRight,faCaretRight,faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faFolder,faAngleLeft,faAngleRight,faCaretRight,faCaretDown)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false